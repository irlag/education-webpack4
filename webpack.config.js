const webpack = require('webpack');
const path = require('path');

//const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const NODE_ENV = process.env.NODE_ENV || 'development';

let config = {
    bail: true,
    devtool: NODE_ENV == 'development' ? 'source-map' : false,
    context: path.resolve(__dirname, 'src/js'),
    entry: {
        app: 'app',
        vendor: ['jquery']
    },
    output: {
        path: path.resolve(__dirname, 'public/assets'),
        //filename: '[name].[chunkhash].js'
        //filename: '[name].[hash].js'
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.mustache'],
        modules: [
            path.resolve(__dirname, 'src'),
            'node_modules'
        ],
        alias: {
            Components: path.resolve(__dirname, 'src/js/components')
        }
    },
    module: {
        strictExportPresence: true,
        rules: [
            // {
            //     test: /\.jsx$/,
            //     exclude: /(node_modules)/,
            //     use: [
            //         {
            //             loader: 'babel-loader',
            //             options: {
            //                 presets: ['@babel/preset-env', '@babel/preset-react'],
            //                 plugins: ['@babel/plugin-transform-runtime']
            //             }
            //         }
            //     ]
            // },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                            // plugins: ['@babel/plugin-transform-runtime']
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                include: /(node_modules)/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {}
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            import: false,
                            url: true,
                            root: './'
                        }
                    }
                ]
            },
            {
                test: /\.mustache$/,
                //loader: 'mustache-loader'
                // loader: 'mustache-loader?minify'
                // loader: 'mustache-loader?{ minify: { removeComments: false } }'
                loader: 'mustache-loader?noShortcut'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                            //publicPath: '/assets/'
                        }
                    }
                ]
            }
        ]
    },
    optimization: {
        splitChunks: {}
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '[name].[contenthash].css',
            //chunkFilename: "[id].css"
        }),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(__dirname, './public/assets'),
            includeAllFileTypes: false,
            fileTypes: ['js', 'css'],
            entrypoints: true
        }),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
            //_: 'lodash'
        }),
        new Dotenv(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    ],
    node: {
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
    }
};

if (NODE_ENV == 'production') {
    config.optimization = {
        minimizer: [
            // new UglifyJsPlugin({
            //     uglifyOptions: {
            //         cache: true,
            //         parallel: true,
            //         sourceMap: true,
            //         beautify: false,
            //         ie8: false,
            //         compress: {
            //             warnings: false,
            //             drop_console: false,
            //             unsafe: true
            //         },
            //         comments: false
            //     }
            // }),
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    ecma: 5,
                    mangle: false,
                    compress: false,
                    keep_classnames: false
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
        // Automatically split vendor and commons
        // https://twitter.com/wSokra/status/969633336732905474
        // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
        splitChunks: {
            chunks: 'all',
            name: false
        },
        // Keep the runtime chunk seperated to enable long term caching
        // https://twitter.com/wSokra/status/969679223278505985
        runtimeChunk: true
    }
} else {
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

    config.plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            openAnalyzer: false
        })
    );
}

module.exports = config;
