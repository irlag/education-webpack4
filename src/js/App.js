import React, {Component} from 'react';

class App extends Component {
    constructor() {
        super();

        const defaultEmail = 'example@email.com';

        this.state = {
            email: defaultEmail
        };

        this.inputEmail = React.createRef();

        this.setEmail = this.setEmail.bind(this);
    }

    setEmail(event) {
        event.preventDefault();

        const currentEmail =  this.inputEmail.current.value;

        this.setState({email: currentEmail})
    }

    render() {
        return (
            <form method="post" action="" onSubmit={this.setEmail}>
                <h1>Email form</h1>
                <input ref={this.inputEmail} type="email" name="email" className="form-input" defaultValue={this.state.email} />
                <input type="submit" value="Send" />
            </form>
        );
    }
}

export default App;
