import moment from 'moment';
import 'moment/locale/ru.js';

class Hello {
    sayHi() {
        alert('Hi, my friend!');
    }
}

export default Hello;
